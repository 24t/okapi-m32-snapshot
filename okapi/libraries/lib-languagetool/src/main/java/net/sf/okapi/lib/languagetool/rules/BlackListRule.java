/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.languagetool.rules;

import java.util.List;

import org.languagetool.Language;
import org.languagetool.rules.patterns.PatternRule;
import org.languagetool.rules.patterns.PatternToken;

/**
 * A Blacklist Check pattern rule class. A BlackListRule describes a language error 
 * where a blacklisted target term is found in the target.
 * @author jimh
 */
public class BlackListRule extends PatternRule {

	public BlackListRule(String id, Language language, List<PatternToken> patternTokens, String description,
			String message, String shortMessage) {
		super(id, language, patternTokens, description, message, shortMessage);
	}

	public BlackListRule(PatternRule trgPattern, String shortMessage) {
		super(trgPattern.getId(), trgPattern.getLanguage(), trgPattern.getPatternTokens(), 
				trgPattern.getDescription(), trgPattern.getMessage(), shortMessage);
	}
}
